from django.conf.urls import url
from .views import index,status


urlpatterns = [
   url(r'^$', index, name='index'),
   url(r'^status/$', index, name='index'),
   url(r'^add_todo/$', status, name='status'),
]

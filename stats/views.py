from django.shortcuts import render
from profile_page.models import Profile
from friend.models import AddFriend
from status.models import Todo

profile = Profile.objects.last()
numb_of_feeds = Todo.objects.count()
numb_of_friends = AddFriend.objects.count()
latest_post = Todo.objects.last()
def index(request):
	response = {'name' : profile.name, 'feeds' : numb_of_feeds, 'friends' : numb_of_friends, 'latestPost' : latest_post}
	return render(request, 'stats.html', response)
# Create your views here.

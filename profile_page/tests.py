from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Profile, Expertise

# Create your tests here.
class UnitTest(TestCase):
    def test_lab_3_url_is_exist(self):
            response = Client().get('/profile_page/')
            self.assertEqual(response.status_code,200)

    def test_model_can_create_new_Profile(self):
        new_activity = Profile.objects.create(name='mhs_name',birthday = 'tanggal&bulan',gender ='female',description='mhs_diri', email='test@gmail.com')
        new_activity.save()
        #Creating a new activity
        expertise = Expertise.objects.create(expertise='mhs_expertise')
        expertise.save()    
        #Retrieving all available activity
        new_activity.expertise.add(expertise)
        
        counting_all_available_Profile= Profile.objects.all().count()
        self.assertEqual(counting_all_available_Profile,1)

    def test_lab4_using_index_func(self):
        found = resolve('/profile_page/')
        self.assertEqual(found.func, index)

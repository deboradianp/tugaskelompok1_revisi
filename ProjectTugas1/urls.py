"""ProjectTugas1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin
import profile_page.urls as profile_page
import status.urls as status
import friend.urls as friend
import stats.urls as stats
from django.views.generic.base import RedirectView

urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^profile_page/', include(profile_page,namespace='profile_page')),
	url(r'^status/', include(status,namespace='status')),
    url(r'^friend/', include(friend,namespace='friend')),
    url(r'^', include('stats.urls', namespace='stats', app_name='stats')),
    url(r'^$', RedirectView.as_view(url = 'status/', permanent = True),name='index')
]
